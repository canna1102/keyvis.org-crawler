from scrapy.selector import HtmlXPathSelector
from scrapy.spiders import BaseSpider
from scrapy.http import FormRequest
from keyvis.items import KeyvisItem
from bs4 import BeautifulSoup

class DmozSpider(BaseSpider):
    name = "keyvis.org"
    start_urls = [
        "http://keyvis.org/expert.php"
    ]

    def start_requests(self):
        for idx in range(0,157):
            expert_idx = str(idx)
            yield FormRequest("http://keyvis.org/load3.php", formdata={"expert": expert_idx}, meta={"expert": expert_idx}, callback = self.after_request)


    def after_request(self, response):
        idx = response.meta["expert"]
        open("web"+idx, 'wb').write(response.body)
        soup = BeautifulSoup(response.body)
        doms = soup.find("div", {"class": "keywords"}).find_all("a")
        items = []
        for dom in doms:
            item = KeyvisItem()
            item['keyword'] = dom.text
            item['index'] =  idx
            items.append(item)
        return items

    # def parse(self, response):
    #     hxs = HtmlXPathSelector(response)
    #     sites = hxs.select('//div/h4')
    #     items = []
    #     for site in sites:
    #         item = KeyvisItem()
    #         item['keyword'] = site.select('a/text()').extract()
    #         item['index'] =  site.select('a/@href').extract()
    #         items.append(item)
    #     return items